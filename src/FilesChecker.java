import java.util.ArrayList;

public class FilesChecker {

    private ArrayList<String> baseFiles;
    private ArrayList<String> currentFiles;
    private Boolean isMismatchFiles;
    private ArrayList<String> diffFiles;

    public FilesChecker() {
        this.baseFiles = new ArrayList<>();
        this.currentFiles = new ArrayList<>();
        this.diffFiles = new ArrayList<>();
        this.isMismatchFiles = false;
        this.checkBaseFilesWithCurrentFiles();
    }

    public void checkBaseFilesWithCurrentFiles() {
        BaseFiles baseFiles = new BaseFiles();
        CurrentFiles currentFiles = new CurrentFiles();
        this.currentFiles.addAll(currentFiles.getCurrentFiles());
        this.isMismatchFiles = !baseFiles.getBaseFiles().containsAll(currentFiles.getCurrentFiles());
    }

    public ArrayList<String> getDiffFiles() {
        if (this.filesMismatch()) {
            for (String diffFile: this.currentFiles) {
                if (!this.getBaseFiles().contains(diffFile)) {
                    this.diffFiles.add(diffFile);
                }
            }
        }

        return this.diffFiles;
    }

    public ArrayList<String> getBaseFiles() {
        BaseFiles baseFiles = new BaseFiles();
        this.baseFiles.addAll(baseFiles.getBaseFiles());

        return this.baseFiles;
    }

    public ArrayList<String> getCurrentFiles() {
        CurrentFiles currentFiles = new CurrentFiles();
        this.currentFiles = currentFiles.getCurrentFiles();

        return this.currentFiles;
    }

    public boolean filesMismatch() {
        return this.isMismatchFiles;
    }
}
