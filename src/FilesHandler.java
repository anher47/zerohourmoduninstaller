import java.io.File;
import java.util.ArrayList;

public abstract class FilesHandler {

    private String[] files;
    private String path;
    private ArrayList<String> subPaths;

    public FilesHandler(String path) {
        this.path = path;
        this.subPaths = new ArrayList<>();
        this.addSubPaths();
    }

    public String getPath() {
        return this.path;
    }

    public File initializedDirectory() {
        return new File(this.getPath());
    }

    public String[] getFiles() {
        return this.files = initializedDirectory().list();
    }

    public void setSubPaths(String subPath) {
        this.subPaths.add(subPath);
    }

    public ArrayList<String> getSubPaths() {
        return this.subPaths;
    }

    public void addSubPaths() {
        for (String file: this.getFiles()) {
            if (!file.contains(".")) {
                this.setSubPaths(file);
            }
        }
    }
}
