import java.io.File;
import java.io.IOException;

public class Main {
    public static String PATH = null;

    public static void main(String[] args) {
        Main main = new Main();
        main.initUi();


    }

    public void removeDiffFiles() {
        FilesChecker filesChecker = new FilesChecker();
        System.out.println("Entered remove action.....");

        if (filesChecker.filesMismatch() && !filesChecker.getDiffFiles().isEmpty()) {
            System.out.println(Main.PATH + "GeneralsZH.ico");
            System.out.println(filesChecker.getCurrentFiles().contains(Main.PATH + "GeneralsZH.ico"));
            for (String filename: filesChecker.getDiffFiles()) {
                File file = new File(filename);
                if (filesChecker.getCurrentFiles().contains(Main.PATH + "\\GeneralsZH.ico")) {
                    if (file.delete()) {
                        System.out.println("Files Deleted successfully!");
                    }
                }
            }
        }
    }

    public void initUi() {
        new MainUi();
    }

}
