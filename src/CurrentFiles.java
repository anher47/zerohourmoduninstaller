import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CurrentFiles extends FilesHandler{

    private ArrayList<String> currentFiles;

    public CurrentFiles() {
        super(Main.PATH);
        this.currentFiles = new ArrayList<>();
        this.setSubFiles();
    }

    public void setCurrentFiles(String currentFiles) {
        this.currentFiles.add(currentFiles);
    }

    public ArrayList<String> getCurrentFiles() {
        return this.currentFiles;
    }

    public void setSubFiles() {
        try (Stream<Path> walk = Files.walk(Paths.get(Main.PATH))) {
            List<String> subPaths = walk.filter(Files::isRegularFile).map(Path::toString).collect(Collectors.toList());
            for (String file: subPaths) {
                this.setCurrentFiles(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
