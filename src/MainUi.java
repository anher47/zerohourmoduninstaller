import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;

public class MainUi extends JFrame {

    public final static int WIDTH = 850;
    public final static int HEIGHT = 200;

    private String selectedDir;
    private JButton browseBtn = new JButton("Browse");
    private JButton removeModeBtn = new JButton("Remove Mode");
    private JLabel header = new JLabel("Select your installation directory");
    private JLabel responseMsg = new JLabel();

    public MainUi() {
        String title = "Zero Hour Mod Uninstaller";
        JPanel mainPanel = new JPanel();
        JPanel msgPanel = new JPanel();

        browseBtn.addActionListener(this.browseAction());
        removeModeBtn.addActionListener(this.removeModeAction());

        this.setVisible(true);
        this.setResizable(false);
        this.setSize(WIDTH, HEIGHT);
        this.setTitle(title);
        this.setBackground(Color.WHITE);
        this.setLayout(new FlowLayout());
        this.removeModeBtn.setEnabled(false);
        this.responseMsg.setVisible(false);
        mainPanel.add(browseBtn);
        mainPanel.add(header);
        mainPanel.add(removeModeBtn);
        msgPanel.setLayout(new FlowLayout());
        msgPanel.add(this.responseMsg);
        this.add(mainPanel);
        this.add(msgPanel);
    }

    private ActionListener browseAction() {

        return e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File("."));
            fileChooser.setDialogTitle("Choose Zero Hour directory");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(false);

            if (fileChooser.showOpenDialog(this.getParent()) == JFileChooser.APPROVE_OPTION) {
                this.selectedDir = fileChooser.getSelectedFile().getAbsolutePath();
                if (this.selectedDir.length() < 95) {
                    this.header.setText(selectedDir);
                } else {
                    this.header.setText("...." + selectedDir.substring(30));
                }
                Main.PATH = selectedDir;
                this.removeModeBtn.setEnabled(true);
            }
        };
    }

    private ActionListener removeModeAction() {

        return e -> {
            if (Main.PATH != null && Main.PATH.contains("Command and Conquer Generals")) {
                FilesChecker filesChecker = new FilesChecker();
                Main main = new Main();

                if (filesChecker.filesMismatch() && !filesChecker.getDiffFiles().isEmpty()) {
                    main.removeDiffFiles();
                    this.responseMsg.setText("The mod was successfully deleted!");
                } else {
                    this.responseMsg.setText("Nothing was deleted. There was no mod found");
                }
            } else {
                this.responseMsg.setText("Please Select a valid Zero Hour path.");
            }
            this.responseMsg.setVisible(true);
        };
    }
}
