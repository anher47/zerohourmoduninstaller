import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BaseFiles extends FilesHandler{

    private ArrayList<String> baseFiles;

    public BaseFiles() {
        super(Main.PATH);
        this.baseFiles = new ArrayList<>();
        this.fileLoader();
    }

    public void setBaseFiles(String baseFile) {
        this.baseFiles.add(baseFile);
    }

    public ArrayList<String> getBaseFiles() {
        return this.baseFiles;
    }

    private void fileLoader() {
        String fileName;
        Scanner fileScan = null;

        File textFile = new File("files.txt");
        try {
            fileScan = new Scanner(textFile).useDelimiter(",\\s*");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (fileScan != null) {
            while (fileScan.hasNext()) {
                fileName = fileScan.next();
                this.setBaseFiles(fileName);
            }

            fileScan.close();
        }
    }
}
